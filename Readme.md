# Generic SCARE

Proof-of-concept for the lossy scenario.
We demonstrate how to recover a whole AES-128 cipher executing on a RISC-V emulator (RV32).

**Require 32 GB of RAM and 100 GB of disk space.**
Computation can take several hours, depends on computing power.

## Install Nix (to do once if not already present on the test machine)

We use [Nix](https://nixos.org/download.html) for a reproducible environment.
To install Nix (multi-user config) on your machine:

```bash
sh <(curl -L https://nixos.org/nix/install) --daemon
```

The scripts will use the Nix configuration [env/shell.nix](./env/shell.nix).
The software used (rustc and julia notably) won't interfere with existing versions on your machine.

## Launching the nix shell

All commands must be entered into the nix shell, launched from the git root folder with
```bash
nix-shell env/shell.nix
```

(Do not add the *--pure** argument at first, since we will need to fetch packages over the internet.)



## Generate trace data

To generate trace data (6M traces), execute the RiscV EMulator (rvem) to generate traces while storing inputs and outputs.

**6M traces does not guarantee a result in 100% of the cases, if it does not work, regenerate data with 7M traces**.

```bash
rvem -i $AES -o $DATA/io -t $DATA/traces_raw.binu8 trace -c 6000000
```

The resulting data can be found in the folder *data*.

## Sample trace data

To simulate a lossy sampling process, we keep only the Hamming weight instead of the values themselves.
To do that:

```
cd apps/perfect2lossy
cargo run --release
```

It creates a new *traceshw_raw.binu8* in the *data* folder.
To ensure that the analysis is not "cheating" you can delete the complete traces (*traces_raw.binu8*).

## Perform the analysis

The analysis is performed by the rust application [lossy-analysis](./apps/lossy-analysis/).
To compile and execute :

```
cd apps/lossy-analysis
cargo run --release
```

The computation will last a few hours, depending on your computer power.
Should require at most 32GB of RAM, the more CPU cores, the better.

There are saved checkpoints during the computation : if you have to interrupt it, it won’t start from the beginning.
Remove the checkpoint files to retrigger the corresponding computation if needed.

At the end of the computation, we use the predictor to verify a hardcoded test vector.
A correct result should end with :

```
PT: 00112233445566778899aabbccddeeff
K: 000102030405060708090a0b0c0d0e0f
Predicted CT: 69c4e0d86a7b0430d8cdb78070b4c55a
True CT: 69c4e0d86a7b0430d8cdb78070b4c55a
SUCCESS: true
```

You can then compute arbitrary ciphertexts by modifying the *pt* variable in *lossy-analysis/src/main.rs* line 429.
The new computations will use your saved checkpoints and be quite fast.
