use std::env;
// use std::path::Path;
use indicatif::ProgressBar;

use bbscare_libs::loaders::trace_matrix::{TraceMatrixLinesBatchReader, TraceMatrixLinesBatchWriter};

const BATCH_SIZE: usize = 10000;
const RAW_TRACES_FILENAME: &str = "traces_raw.binu8";
const RAW_TRACESHW_FILENAME: &str = "traceshw_raw.binu8";

fn leakage_model(b: u8) -> u8 {
    b.count_ones() as u8
}

fn get_data_file(filename: &str) -> String {
    let data_folder: String = match env::var("DATA") {
        Ok(d) => d,
        Err(_) => "../../data".to_owned()
    };
    return data_folder + "/" + filename;
}

fn main() -> anyhow::Result<()> {
    let mut perfect_raw_traces = TraceMatrixLinesBatchReader::new(get_data_file(RAW_TRACES_FILENAME), BATCH_SIZE)?;

    let traces_count = perfect_raw_traces.traces_count;
    let traces_len = perfect_raw_traces.traces_len;

    let mut model_raw_traces = TraceMatrixLinesBatchWriter::new(get_data_file(RAW_TRACESHW_FILENAME), traces_len, traces_count)?;

    let progress = ProgressBar::new(perfect_raw_traces.steps() as u64);
    while !perfect_raw_traces.is_terminated() { // while there is a batch
        let perfect_lines = perfect_raw_traces.read_batch()?;
        let mut hw_lines: Vec<Vec<u8>> = Vec::with_capacity(traces_count);

        for perfect_line in perfect_lines.iter() { //for each line in the batch
            let mut hw_line: Vec<u8> = Vec::with_capacity(traces_len);
            for b in perfect_line.iter() { //for each byte in the line
                hw_line.push(leakage_model(*b));
            }
            hw_lines.push(hw_line);
        }

        model_raw_traces.write_batch(&hw_lines)?;
        progress.inc(1);
    }
    progress.finish();

    // delete perfect traces

    Ok(())
}
