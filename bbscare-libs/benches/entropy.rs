use bbscare_libs::loaders::file_storable::FileStorable;
use criterion::{black_box, criterion_group, criterion_main, Criterion};
use bbscare_libs::information::distribution::{Distribution, Distribution2_8, Distribution2_16, Distribution2_24Dense, Distribution2_24Sparse};
use bbscare_libs::information::entropies::{EntropyEstimator, shannon_entropy3, fractionnal_added_information_2to1, fractionnal_added_information_1to1};

pub fn entropy28(c: &mut Criterion) {
    let vec = Distribution2_8::samples_uniform(100000);
    let distrib = Distribution2_8::from_vector(&vec);
    c.bench_function("entropy 2^8", |b| b.iter(|| black_box(distrib.clone()).shannon_entropy()));
}

pub fn entropy216(c: &mut Criterion) {
    let vec1 = Distribution2_8::samples_uniform(100000);
    let vec2 = Distribution2_8::samples_uniform(100000);
    let distrib = Distribution2_16::from_2_vectors(&vec1, &vec2).unwrap();
    c.bench_function("entropy 2^16", |b| b.iter(|| black_box(distrib.clone()).shannon_entropy()));
}

pub fn entropy224_dense(c: &mut Criterion) {
    let vec1 = Distribution2_8::samples_uniform(100000);
    let vec2 = Distribution2_8::samples_uniform(100000);
    let vec3 = Distribution2_8::samples_uniform(100000);
    let distrib = Distribution2_24Dense::from_3_vectors(&vec1, &vec2, &vec3).unwrap();
    c.bench_function("entropy 2^24 dense", |b| b.iter(|| black_box(distrib.clone()).shannon_entropy()));
}

pub fn distrib224_dense(c: &mut Criterion) {
    let vec1 = Distribution2_8::samples_uniform(100000);
    let vec2 = Distribution2_8::samples_uniform(100000);
    let vec3 = Distribution2_8::samples_uniform(100000);
    c.bench_function("distrib 2^24 dense", |b| b.iter(||  Distribution2_24Dense::from_3_vectors(&vec1, &vec2, &vec3).unwrap()));
}

pub fn entropy224_sparse(c: &mut Criterion) {
    let vec1 = Distribution2_8::samples_uniform(100000);
    let vec2 = Distribution2_8::samples_uniform(100000);
    let vec3 = Distribution2_8::samples_uniform(100000);
    let distrib = Distribution2_24Sparse::from_3_vectors(&vec1, &vec2, &vec3).unwrap();
    c.bench_function("entropy 2^24 sparse", |b| b.iter(|| black_box(distrib.clone()).shannon_entropy()));
}

pub fn distrib224_sparse(c: &mut Criterion) {
    let vec1 = Distribution2_8::samples_uniform(100000);
    let vec2 = Distribution2_8::samples_uniform(100000);
    let vec3 = Distribution2_8::samples_uniform(100000);
    c.bench_function("distrib 2^24 sparse", |b| b.iter(||  Distribution2_24Sparse::from_3_vectors(&vec1, &vec2, &vec3).unwrap()));
}

fn estimator_table_generation(c: &mut Criterion) {
    c.bench_function("estimator table generation", |b| b.iter(||  {
        let mut estimator = EntropyEstimator::new();
        estimator.add_estimator(1000,2);
        estimator
    }));
}

fn fai2to1_100000(c: &mut Criterion) {
    let estimator = EntropyEstimator::from_file("entropy_estimators").unwrap();
    let vec1 = Distribution2_8::samples_uniform(100000);
    let vec2 = Distribution2_8::samples_uniform(100000);
    let vec3 = Distribution2_8::samples_uniform(100000);

    c.bench_function("fai2to1", |b| b.iter(|| {
        fractionnal_added_information_2to1(&vec1, &vec2, &vec3, &estimator)
    }));
}

fn fai1to1_100000(c: &mut Criterion) {
    let estimator = EntropyEstimator::from_file("entropy_estimators").unwrap();
    let vec1 = Distribution2_8::samples_uniform(100000);
    let vec2 = Distribution2_8::samples_uniform(100000);

    c.bench_function("fai1to1", |b| b.iter(|| {
        fractionnal_added_information_1to1(&vec1, &vec2, &estimator)
    }));
}

// criterion_group!(benches, entropy28, entropy216, entropy224_dense, distrib224_dense, entropy224_sparse, distrib224_sparse);
// criterion_group!(benches, estimator_table_generation);
criterion_group!(benches, entropy28);
criterion_main!(benches);