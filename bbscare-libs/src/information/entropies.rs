use serde::{Deserialize, Serialize};

use crate::loaders::file_storable::FileStorable;

use super::distribution::{Distribution2_8, Distribution, Distribution2_16, Distribution2_24Sparse};


pub fn shannon_entropy1(vec: &[u8], estimator: &EntropyEstimator) -> anyhow::Result<f32> {
    let number_of_samples = vec.len();
    let distrib = Distribution2_8::from_vector(vec);
    let measured_entropy = distrib.shannon_entropy();

    match estimator.estimate_entropy(measured_entropy, number_of_samples, 1) {
        Some(f) => Ok(f),
        None => Err(anyhow::format_err!("No estimator for {} samples and 1 dimension.", number_of_samples))
    }
}

pub fn shannon_entropy2(vec1: &[u8], vec2: &[u8], estimator: &EntropyEstimator) -> anyhow::Result<f32> {
    let number_of_samples = vec1.len();
    let distrib = Distribution2_16::from_2_vectors(vec1, vec2)?;
    let measured_entropy = distrib.shannon_entropy();

    match estimator.estimate_entropy(measured_entropy, number_of_samples, 2) {
        Some(f) => Ok(f),
        None => Err(anyhow::format_err!("No estimator for {} samples and 2 dimensions.", number_of_samples))
    }
}

pub fn shannon_entropy3(vec1: &[u8], vec2: &[u8], vec3: &[u8], estimator: &EntropyEstimator) -> anyhow::Result<f32> {
    let number_of_samples = vec1.len();
    let distrib = Distribution2_24Sparse::from_3_vectors(vec1, vec2, vec3)?;
    let measured_entropy = distrib.shannon_entropy();

    match estimator.estimate_entropy(measured_entropy, number_of_samples, 3) {
        Some(f) => Ok(f),
        None => Err(anyhow::format_err!("No estimator for {} samples and 3 dimensions.", number_of_samples))
    }
}

pub fn fractionnal_added_information_1to1(testing: &[u8], knowing: &[u8], estimator: &EntropyEstimator) -> anyhow::Result<f32> {
    let testing_se = shannon_entropy1(testing, estimator)?;
    if testing_se < 0.001f32 {
        return Ok(1f32);
    }
    else {
        let ce = shannon_entropy2(testing, knowing, estimator)? - shannon_entropy1(knowing, estimator)?;
        return Ok(ce/testing_se);
    }
}

pub fn fractionnal_added_information_2to1(testing: &[u8], knowing1: &[u8], knowing2: &[u8], estimator: &EntropyEstimator) -> anyhow::Result<f32> {
    let testing_se = shannon_entropy1(testing, estimator)?;
    if testing_se < 0.001f32 {
        return Ok(1f32);
    }
    else {
        let ce = shannon_entropy3(testing, knowing1, knowing2, estimator)? - shannon_entropy2(knowing1, knowing2, estimator)?;
        return Ok(ce/testing_se);
    }
}

pub fn fractionnal_added_information_2to1_memoize_knowing(testing: &[u8], knowing1: &[u8], knowing2: &[u8], knowing_entropy: f32, estimator: &EntropyEstimator) -> anyhow::Result<f32> {
    let testing_se = shannon_entropy1(testing, estimator)?;
    if testing_se < 0.001f32 {
        return Ok(1f32);
    }
    else {
        let ce = shannon_entropy3(testing, knowing1, knowing2, estimator)? - knowing_entropy;
        return Ok(ce/testing_se);
    }
}

#[derive(Debug,Clone,Serialize,Deserialize)]
pub struct EntropyEstimatorTable {
    // entropies: Vec<(f32, f32)>, // (measured entropy, read entropy)
    measured_entropies: Vec<f32>,
    real_entropies: Vec<f32>,
}

impl EntropyEstimatorTable {
    pub fn generate_entropy_estimator_table(number_of_samples: usize, number_of_dims: usize) -> EntropyEstimatorTable {
        assert!(number_of_dims <= 3);
        let mut measured_entropies = Vec::with_capacity(256);
        let mut real_entropies = Vec::with_capacity(256);
        const AVERAGING: usize = 5;

        for zeros in (0..256).rev() { // the number of zeros is used to generate "degraded uniform" distributions

            let mut average_real_entropy: f32 = 0f32;
            let mut average_measured_entropy: f32 = 0f32;

            for _ in 0..AVERAGING {
                // first we generate distribution
                let distrib_with_zeros = Distribution2_8::uniform_ending_with_zeros(zeros);
                let mut samples: Vec<Vec<u8>> = Vec::with_capacity(number_of_dims);

                for _ in 0..number_of_dims { // we create a vector of bytes for each dimension
                    let v = distrib_with_zeros.samples(number_of_samples);
                    samples.push(v);
                }

                // now we evaluate entropies

                let real_entropy: f32 = distrib_with_zeros.shannon_entropy() * (number_of_dims as f32); // vectors are iid
                let measured_entropy: f32 = match number_of_dims {
                    1 => {
                        let measured_dist = Distribution2_8::from_vector(&samples[0]);
                        measured_dist.shannon_entropy() 
                    },
                    2 => {
                        let measured_dist = Distribution2_16::from_2_vectors(&samples[0], &samples[1]).unwrap();
                        measured_dist.shannon_entropy() 
                    },
                    3 => {
                        let measured_dist = Distribution2_24Sparse::from_3_vectors(&samples[0], &samples[1], &samples[2]).unwrap();
                        measured_dist.shannon_entropy()
                    },
                    _ => unreachable!() // assert should prevent this
                };

                average_real_entropy += real_entropy;
                average_measured_entropy += measured_entropy;
            }

            measured_entropies.push(average_measured_entropy / (AVERAGING as f32));
            real_entropies.push(average_real_entropy / (AVERAGING as f32));
        }

        return EntropyEstimatorTable { measured_entropies, real_entropies };
    }

    pub fn estimate_entropy(&self, measured_entropy: f32) -> f32 {
        // corner cases (we can unwrap since table can only exists populated)
        if measured_entropy < self.measured_entropies[0] {
            return self.real_entropies[0];
        }

        if measured_entropy > self.measured_entropies[255] {
            return self.real_entropies[255];
        }

        // binary search
        let mut lower_index = 0;
        let mut higher_index = 255;

        let mut lower_measured_entropy = 0f32;
        let mut higher_measured_entropy = 0f32;

        while (higher_index - lower_index) > 1 {
            let mid_index = lower_index + ((higher_index - lower_index) / 2);
            let mid_measured_entropy = self.measured_entropies[mid_index];

            if mid_measured_entropy <= measured_entropy {
                lower_index = mid_index;
                lower_measured_entropy = mid_measured_entropy;
            }
            else {
                higher_index = mid_index;
                higher_measured_entropy = mid_measured_entropy;
            }
        }

        // debug
        // println!("Lower measured entropy ({}): {}", lower_index, lower_measured_entropy);
        // println!("Lower real entropy ({}): {}", lower_index, self.real_entropies[lower_index]);
        // println!("Higher measured entropy ({}): {}", higher_index, higher_measured_entropy);
        // println!("Higher real entropy ({}): {}", higher_index, self.real_entropies[higher_index]);

        // now linear extrapolation
        let alpha = (measured_entropy - lower_measured_entropy) / (higher_measured_entropy - lower_measured_entropy);
        // println!("Alpha: {}", alpha);
        return self.real_entropies[lower_index] + alpha * (self.real_entropies[higher_index] - self.real_entropies[lower_index]);
    }
}

#[derive(Debug,Clone,Serialize,Deserialize)]
pub struct EntropyEstimator {
    estimator: std::collections::HashMap<(usize, usize), EntropyEstimatorTable> // (number of samples, number of dimensions) -> Table
}


impl EntropyEstimator {

    pub fn new() -> EntropyEstimator {
        EntropyEstimator { estimator: std::collections::HashMap::new() }
    }

    pub fn add_estimator(&mut self, number_of_samples: usize, number_of_dims: usize) -> bool {
        // test existence
        if self.estimator.get(&(number_of_samples, number_of_dims)).is_none() {
            let est = EntropyEstimatorTable::generate_entropy_estimator_table(number_of_samples, number_of_dims);
            self.estimator.insert((number_of_samples, number_of_dims), est);
            true
        }
        else {
            false
        }
    }

    /// Estimate entropy if estimator exists
    pub fn estimate_entropy(&self, measured_entropy: f32, number_of_samples: usize, number_of_dims: usize) -> Option<f32> {
        self.estimator.get(&(number_of_samples, number_of_dims)).and_then(|t|Some(t.estimate_entropy(measured_entropy)))
    }

    pub fn get_estimators(&self) -> &std::collections::HashMap<(usize, usize), EntropyEstimatorTable> {
        &self.estimator
    }

    pub fn list_estimators_parameters(&self) -> Vec<(usize, usize)> {
        let mut params = Vec::new();
        for p in self.estimator.keys() {
            params.push(*p);
        }
        return params;
    }
}

impl FileStorable for EntropyEstimator {}

#[cfg(test)]
mod tests {
// #[test]
    // fn test_estimator_table() {
    //     let estimator_table = EntropyEstimatorTable::generate_entropy_estimator_table(10000, 2);

    //     let vec1 = Distribution2_8::samples_uniform(10000);
    //     let vec2 = Distribution2_8::samples_uniform(10000);
    //     let distrib = Distribution2_16::from_2_vectors(&vec1, &vec2).unwrap();
    //     let se = distrib.shannon_entropy();

    //     assert!((se - 13.1f32).abs() < 0.1f32, "Shannon entropy is {}", se); 
    //     let corrected_se = estimator_table.estimate_entropy(se);
    //     assert!((corrected_se - 16f32).abs() < 0.1f32, "Shannon entropy is {}", corrected_se); 
    // }

    // #[test]
    // fn test_estimator_manager() {
    //     let mut estimator = EntropyEstimator::new();
    //     estimator.add_estimator(1000,2);
    //     estimator.save_to_file("entropy_estimator").unwrap();

    //     let estimator2 = EntropyEstimator::from_file("entropy_estimator").unwrap();
    //     let e = estimator2.estimate_entropy(8f32, 1000, 2).unwrap();
    //     assert!((e - 8.2f32).abs() < 0.1f32, "Shannon entropy is {}", e);
    // }
}