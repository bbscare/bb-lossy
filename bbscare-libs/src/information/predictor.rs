use serde::{Serialize, Deserialize};

use crate::loaders::file_storable::FileStorable;

use super::{io_functions::{IOFunctions}, functions::Functions, hgraph::{HGraph, HEdgeInput}};

#[derive(Debug,Clone, Serialize, Deserialize)]
pub struct Predictor {
    io_functions: IOFunctions,
    functions: Functions,
    logic_hg: HGraph
}

impl Predictor {
    pub fn from_functions(io_functions: IOFunctions, functions: Functions, logic_hg: HGraph) -> Predictor {
        Predictor { io_functions, functions, logic_hg }
    }

    pub fn predict(&self, input_vector: &Vec<u8>) -> anyhow::Result<Vec<u8>> {
        let mut logic_trace: Vec<Option<u8>> = vec![None;self.functions.logic_trace_len];

        // inputs -> trace
        for (i, input_val) in input_vector.iter().enumerate() {
            logic_trace[self.io_functions.inputs_mapping[i]] = Some(*self.io_functions.input_funcs[i].get(&input_val).unwrap());
        }

        //trace in trace
        for edge_output in 0..self.functions.logic_trace_len {
            if logic_trace[edge_output].is_none() {
                let edges = self.logic_hg.edges_ending_in(edge_output);

                // debug
                if edge_output == 540 {
                    println!("Edges 540 {:?}", edges);
                }

                for (edge, _) in edges.iter() {
                    match edge.inputs {
                        HEdgeInput::Two(i1, i2) => {
                            // if logic_trace[i1].is_none() {
                            //     println!("({}, {}) -> {}", i1, i2, edge_output);
                            // }
                            let v1 = logic_trace[i1].unwrap(); // order should ensure safe unwrap
                            let v2 = logic_trace[i2].unwrap();

                            // let v3 = self.functions.apply(edge, v1, v2).unwrap(); // panic if not valid
                            let v3 = match self.functions.apply(edge, v1, v2) {
                                Some(u) => u,
                                None => {panic!("({}, {}) -> {} for values ({}, {})\n{:?}", i1, i2, edge_output, v1, v2, self.functions.funcs.get(edge).unwrap().len());}
                            };
                            
                            match logic_trace[edge_output] {
                                Some(v3check) => {
                                    if v3 != v3check {
                                        return Err(anyhow::format_err!("Error for logic time {}, functions are incoherent", edge_output));
                                    }
                                },
                                None => {
                                    logic_trace[edge_output] = Some(v3);
                                }
                            }
                        },
                        _ => {return Err(anyhow::format_err!("Only 2to1 functions in logic traces."));}
                    }
                }

            }
        }

        let output_len = self.io_functions.outputs_mapping.len();
        let mut output: Vec<u8> = vec![0; output_len];
        // println!("{:?}", logic_trace);

        // trace -> output
        for output_byte in 0..output_len {
            let trace_time = self.io_functions.outputs_mapping[output_byte];
            if let Some(trace_value) = logic_trace[trace_time] {
                output[output_byte] = self.io_functions.apply_output(output_byte, trace_value)?;
            }
            else {
                if logic_trace[trace_time].is_none() {
                    println!("No value at {}", trace_time);
                }

                match self.io_functions.output_funcs.get(output_byte) {
                    None => {println!("No function.");},
                    Some(_) => {
                        
                    }
                }
                return Err(anyhow::format_err!("Cannot predict output byte {} at trace time {}.", output_byte, trace_time));
            }            
        }

        Ok(output)
    }
}

impl FileStorable for Predictor {}

// fn count_uniques(vec: &[u8]) -> usize {
//     let mut v: Vec<u8> = vec.to_vec();
//     v.sort();
//     v.dedup();
//     v.len()
// }