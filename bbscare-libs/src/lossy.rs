use hashbrown::{HashMap, HashSet};
use anyhow::Context;
use crate::{loaders::trace_matrix::{TraceMatrixColumns}, information::hgraph::{HGraph, HEdgeInput}};

#[derive(Debug,Clone,Copy)]
pub enum DontCareByte {
    Byte(u8),
    DontCare
}

impl PartialEq for DontCareByte {
    fn eq(&self, other: &Self) -> bool {
        match self {
            DontCareByte::DontCare => true,
            DontCareByte::Byte(b1) => {
                match other {
                    DontCareByte::DontCare => true,
                    DontCareByte::Byte(b2) => b1 == b2
                }
            }
        }
    }
}

pub fn fuse(main: &mut Vec<DontCareByte>, other: &Vec<DontCareByte>) -> anyhow::Result<()> {
    assert!(main.len() == other.len());

    for i in 0..main.len() {
        match main[i] {
            DontCareByte::Byte(bmain) => {
                match other[i] {
                    DontCareByte::Byte(bother) => {
                        if bmain != bother {
                            return Err(anyhow::format_err!("Incompatible vectors cannot be fused."));
                        }
                    },
                    _ => {}
                }
            },
            DontCareByte::DontCare => {
                main[i] = other[i];
            }
        }
    }

    Ok(())
}

impl Eq for DontCareByte {}

pub trait WealthPattern {
    // fn enrich(&self, traces: &mut TraceMatrixLines, logic_hg: &HGraph);
    fn is_valid(&self, enriched: &Vec<bool>) -> bool;
}

// The wealth pattern is a collection of 5 logic times, with dedicated purposes
#[derive(Debug,Clone)]
pub struct WealthPattern1 {
    //logic times
    //parents
    parent1: usize, //is rich
    parent2: usize, //is rich
    //focus
    focus: usize, //trying to enrich, maybe poor
    //children (children can, and must in some cases, be switched)
    child1: usize, //is rich
    child2: usize, //maybe poor
}

impl WealthPattern for WealthPattern1 {
    fn is_valid(&self, enriched: &Vec<bool>) -> bool {
        return enriched[self.parent1] && enriched[self.parent2] && enriched[self.child1];
    }
}

impl WealthPattern1 {
    //assume pattern is respected
    // enriched : mask vector of logic times : true if already rich at that time
    pub fn enrich(&self, poor_logic_traces: &mut TraceMatrixColumns) -> anyhow::Result<()> {
        let obs = self.observation_vectors(poor_logic_traces).context(format!("For pattern {:?}", self))?;
        let sets = sets_from_observations(&obs).context(format!("For pattern {:?}", self))?;

        let mut set_assign: HashMap<(u8,u8), u8> = HashMap::new();
        for (si, set) in sets.iter().enumerate() {
            for p in set.iter() {
                set_assign.insert(*p, si as u8);
            }
        }

        let traces_count = poor_logic_traces.traces_count();
        let parent1 = poor_logic_traces.column(self.parent1);
        let parent2 = poor_logic_traces.column(self.parent2);
        let mut new_focus: Vec<u8> = vec![0; traces_count];

        for i in 0..traces_count {
            new_focus[i] = *set_assign.get(&(parent1[i], parent2[i])).unwrap();
        }

        poor_logic_traces.write_column(self.focus, new_focus);
        Ok(())
    }

    fn observation_vectors(&self, poor_logic_traces: &TraceMatrixColumns) -> anyhow::Result<HashMap<(u8, u8), Vec<DontCareByte>>> {
    
        let mut obs: HashMap<(u8, u8), Vec<DontCareByte>> = HashMap::new();
        let t4vals_count = {
            let set: HashSet<_> = poor_logic_traces.column(self.child1).iter().collect();
            set.len()
        }; 

        //assume t4 values are < t4vals_count

        let first_order_index = t4vals_count;
        let parent1 = poor_logic_traces.column(self.parent1);
        let parent2 = poor_logic_traces.column(self.parent2);
        let focus = poor_logic_traces.column(self.focus);
        let child1 = poor_logic_traces.column(self.child1);
        let child2 = poor_logic_traces.column(self.child2);
    
        for i in 0..poor_logic_traces.traces_count() {
            let k = (parent1[i], parent2[i]);
    
            if !obs.contains_key(&k) {
                obs.insert(k, vec![DontCareByte::DontCare; t4vals_count + 1]);//we have one observation per value in child1
            }
            match obs.get_mut(&k) {
                Some(vec) => {
                    let focus_val = focus[i];
                    let child1_val = child1[i];
                    assert!((child1_val as usize) < t4vals_count);
                    let child2_val = child2[i];

                    //first order
                    match vec[first_order_index] {
                        DontCareByte::DontCare => { vec[first_order_index] = DontCareByte::Byte(focus_val); },
                        DontCareByte::Byte(b) => { // this is a consistency check only
                            if b != focus_val {
                                return Err(anyhow::format_err!("Inputs (focus) not consistent for k={:?} and b={}", k, b));
                            }
                        }
                    }

                    //second order
                    match vec[child1_val as usize] {
                        DontCareByte::DontCare => { //this is a consistency check
                            vec[child1_val as usize] = DontCareByte::Byte(child2_val);
                        },
                        DontCareByte::Byte(b) => {
                            if b != child2_val {
                                return Err(anyhow::format_err!("Inputs (child2) not consistent for k={:?} and b={}", k, b));
                            }
                        }
                    }
                },
                None => {
                    unreachable!() // we test and create key before
                }
            }
        }
    
        Ok(obs)
    }


}


fn sets_from_observations(obs: & HashMap<(u8, u8), Vec<DontCareByte>>) -> anyhow::Result<Vec<HashSet<(u8, u8)>>> {
    let mut sets: Vec<(HashSet<(u8, u8)>, Vec<DontCareByte>)> = Vec::new();

    for (parent_values, obs_vector) in obs.iter() {
        let mut compatible_sets_count: usize = 0;

        for (parent_set, fused_obs_vector) in sets.iter_mut() {
            if obs_vector == fused_obs_vector {
                //fuse
                parent_set.insert(*parent_values);
                fuse(fused_obs_vector, obs_vector)?;
                compatible_sets_count += 1;
            }
        }

        if compatible_sets_count == 0 {
            // create new set
            let parent_set: HashSet<(u8, u8)> = vec![*parent_values].into_iter().collect();
            sets.push((parent_set, obs_vector.clone()));
        }

        if sets.len() > 256 {
            return Err(anyhow::format_err!("Enrichment failed: too many sets. ({})", sets.len()));
        }
    }

    return Ok(sets.into_iter().map(|(s, _)| s).collect());
}


pub fn wealth_patterns1_at(logic_t: usize, logic_hg: &HGraph, enriched: &Vec<bool>) -> Vec<WealthPattern1> {
    let mut wealth_patterns = Vec::new();

    let parents_edges = logic_hg.edges_ending_in(logic_t);

    // keeping only rich parents
    let mut rich_parents_times: Vec<(usize, usize)> = Vec::new();
    for (pedge, _) in parents_edges {
        match pedge.inputs {
            HEdgeInput::Two(i1, i2) => {
                if enriched[i1] && enriched[i2] {
                    rich_parents_times.push((i1, i2));
                }
            },
            HEdgeInput::One(_) => { unreachable!() }
        }
    }

    if rich_parents_times.is_empty() {
        return wealth_patterns;
    }

    let children_edges = logic_hg.edges_starting_from(logic_t);
    let mut children_pattern: Vec<(usize, usize)> = Vec::new();

    // keeping correct child pattern
    for (cedge, _) in children_edges {
        
        let c2: usize = cedge.output;
        let c1: usize = match cedge.inputs {
            HEdgeInput::Two(x1, x2) => {
                if x1 == logic_t {
                    x2
                }
                else {
                    x1
                }
            },
            HEdgeInput::One(_) => {unreachable!()}
        };

        if enriched[c1] {
            children_pattern.push((c1, c2));
        }

        if enriched[c2] { // swap in this case
            children_pattern.push((c2, c1));
        }
    }

    for (p1, p2) in rich_parents_times {
        for (c1, c2) in children_pattern.iter() {
            wealth_patterns.push(WealthPattern1 { parent1: p1, parent2: p2, focus: logic_t, child1: *c1, child2: *c2});
        }
    }

    wealth_patterns
}

pub fn count_uniques(vec: &[u8]) -> usize {
    let mut v: Vec<u8> = vec.to_vec();
    v.sort();
    v.dedup();
    v.len()
}

pub fn smart_enrich(poor_logic_traces_columns: &mut TraceMatrixColumns, logic_hg: &HGraph, enriched: &mut Vec<bool>) -> anyhow::Result<()> {
    
    
    let traces_len = poor_logic_traces_columns.traces_len();
    
    let mut pass_number = 0;
    loop {
        let mut enriched_count = 0;
        

        for t in 0..traces_len {
            if enriched[t] == false {
                let valid_patterns = wealth_patterns1_at(t, logic_hg, enriched);

                for pattern in valid_patterns {
                    match pattern.enrich(poor_logic_traces_columns) {
                        Ok(()) => {
                            enriched_count += 1;
                            // println!("{} enriched.", t);
                            enriched[t] = true;
                        },
                        Err(e) => {
                            // println!("For pattern {:?}, {}", pattern, e);
                            println!("{}", e);
                            println!("Child1 count = {}, child2 count = {}", 
                                count_uniques(poor_logic_traces_columns.column(pattern.child1)),
                                count_uniques(poor_logic_traces_columns.column(pattern.child2)));
                        }
                    }
                    
                }
            }
        }

        println!("Pass {}, enriched columns: {}", pass_number, enriched_count);


        if enriched_count == 0 {
            break;
        }
        pass_number += 1;
    }

    Ok(())
}