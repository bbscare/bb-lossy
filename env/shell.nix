with import <nixpkgs> {};


let
     pkgs = import (builtins.fetchGit {
         # Descriptive name to make the store path easier to identify                
         name = "nixos-22-11";
        url = "https://github.com/nixos/nixpkgs/";
        ref = "refs/heads/nixos-22.11";
        rev = "ab1254087f4cdf4af74b552d7fc95175d9bdbb49";                                           
     }) {};                            

    rvem = (import ../packages/rvem.nix);                            
in

stdenv.mkDerivation {
    name = "bb-perfect-env";

    DATA = toString ../data;
    LIBS = toString ../libs;
    AES = toString ../targeted-algorithms/aes/aes.s;
    TWINE = toString ../targeted-algorithms/twine/twine.s;

    # The packages in the `buildInputs` list will be added to the PATH in our shell
    nativeBuildInputs = with pkgs; [
        rvem
        cargo
        rustc
    ];


    shellHook = ''
        mkdir -p data
    '';
}