/*
 * File: mc.s
 * Project: aes
 * Created Date: Thursday February 13th 2020
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 19th February 2020 11:51:03 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2020 INRIA
 */

#[global]
@mixcolumns:

    push ra;

    a0 <- s0;
    call &mc;
    s0 <- a0;

    a0 <- s1;
    call &mc;
    s1 <- a0;

    a0 <- s2;
    call &mc;
    s2 <- a0;

    a0 <- s3;
    call &mc;
    s3 <- a0;

    pop ra;
    return;
    



@mc://input and output in a0
    push ra;

    //1*
    t0 <- a0;
    t0 <- t0 & 0xFF;

    s7 <- t0;
    s6 <- t0;

    t1 <- a0 >> 8;
    t1 <- t1 & 0xFF;

    s7 <- s7 xor t1;
    s4 <- t1;

    t2 <- a0 >> 16;
    t2 <- t2 & 0xFF;

    s5 <- t2;
    s4 <- s4 xor t2;

    t3 <- a0 >> 24;
    t3 <- t3 & 0xFF;

    s6 <- s6 xor t3;
    s5 <- s5 xor t3;

    //2* and 3*

    a0 <- t0;
    s8 <- t0;
    call &xtimes;
    s4 <- s4 xor a0;

    a0 <- a0 xor s8;//3*
    s5 <- s5 xor a0;

    
    a0 <- t1;
    call &xtimes;
    s5 <- s5 xor a0;

    a0 <- a0 xor t1;
    s6 <- s6 xor a0;


    a0 <- t2;
    call &xtimes;
    s6 <- s6 xor a0;

    a0 <- a0 xor t2;
    s7 <- s7 xor a0;


    a0 <- t3;
    call &xtimes;
    s7 <- s7 xor a0;

    a0 <- a0 xor t3;
    s4 <- s4 xor a0;

    //build a0 = s4 << 24 xor ... xor s7
    a0 <- s7 << 24;
    
    s6 <- s6 << 16;
    a0 <- a0 xor s6;

    s5 <- s5 << 8;
    a0 <- a0 xor s5;

    a0 <- a0 xor s4;

    pop ra;
    return;


@xtimes:
    a0 <- a0 << 1;
    t0 <- 0xFF;
    if (a0 <u t0) goto &nomodulo;
@modulo:
        t0 <- 0x11B;
        goto &endxtimes;
@nomodulo:
        t0 <- 0x0;
        goto &endxtimes;
@endxtimes:
    a0 <- a0 xor t0;
    return;