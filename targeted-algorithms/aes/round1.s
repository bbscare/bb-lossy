/*
 * File: round1.s
 * Project: aes
 * Created Date: Friday November 29th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 22nd April 2021 9:35:54 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

import "ark.s";
import "sb.s";
import "sr.s";
import "mc.s";

//memory layout
// @0x1000: plaintext up to 0x100F
// @0x1010: key up to 0x101F
// @0x1020: ciphertext up to 0x102F



// State is stored on s0, s1, s2, s3
//#[address(0xA0000)]
//@cipher_data:

//assume row encoded

//def pt1: u32 = 0x00112233;
//def pt2: u32 = 0x44556677;
//def pt3: u32 = 0x8899AABB;
//def pt4: u32 = 0xCCDDEEFF;


#[address(0xB0000)]
@key_data:

def k1: u32 = 0x00010203;
def k2: u32 = 0x04050607;
def k3: u32 = 0x08090A0B;
def k4: u32 = 0x0C0D0E0F;


//init
#[address(0x8000)]
@entry:

//init stack
sp <- 0x3000;

//t0 <-t1 [&pt1]; //load pt1 in t0
//[0x1000] <-t1 t0;//store t0 at 0x1000
//t0 <-t1 [&pt2];
//[0x1004] <-t1 t0;
//t0 <-t1 [&pt3];
//[0x1008] <-t1 t0;
//t0 <-t1 [&pt4];
//[0x100C] <-t1 t0;

t0 <-t1 [&k1];
[0x1010] <-t1 t0;
t0 <-t1 [&k2];
[0x1014] <-t1 t0;
t0 <-t1 [&k3];
[0x1018] <-t1 t0;
t0 <-t1 [&k4];
[0x101C] <-t1 t0;

// init state with constant
//s0 <-t0 [0x1000];
//s1 <-t0 [0x1004];
//s2 <-t0 [0x1008];
//s3 <-t0 [0x100C];

//init with prng
s0 <- CSR[0x123];
s1 <- CSR[0x123];
s2 <- CSR[0x123];
s3 <- CSR[0x123];

call &ark;

call &subbytes;

call &shiftrows;

call &mixcolumns;

//save output
output s0;
output s1;
output s2;
output s3;
CSR[0] <- x0;//halt with error